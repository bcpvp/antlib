package dev.bcfcant.juicifruit.antlib.core.chat.censor.filters;

import org.bukkit.entity.Player;

import dev.bcfcant.juicifruit.antlib.core.chat.censor.ChatCensor.ChatFilter;

public class CapsFilter implements ChatFilter {

	@Override
	public String filter(Player player, String msg) {
		StringBuilder sb = new StringBuilder();
		String[] words = msg.split(" ");
		for (String word : words) {
			int length = word.length();
			String s = word;
			if (length > 2) {
				int caps = 0;
				for (int i = 0; i < length; i++) {
					if (Character.isUpperCase(word.charAt(i)))
						caps++;
				}
				if (caps > length / 2)
					s = word.toLowerCase();
			}
			sb.append(s).append(" ");
		}
		return sb.toString();
	}
}
