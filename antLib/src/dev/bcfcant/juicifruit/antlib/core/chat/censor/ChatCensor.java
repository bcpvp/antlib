package dev.bcfcant.juicifruit.antlib.core.chat.censor;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.core.chat.censor.filters.CapsFilter;
import dev.bcfcant.juicifruit.antlib.core.chat.censor.filters.SwearFilter;

public class ChatCensor {

	private static Set<ChatFilter> filters = new HashSet<ChatFilter>();

	public static void init(AntLib plugin) {
		filters.add(new SwearFilter(plugin));
		filters.add(new CapsFilter());
	}

	public static String filter(Player player, String msg) {
		for (ChatFilter filter : filters) {
			msg = filter.filter(player, msg);
			if (msg == null)
				break;
		}
		return msg;
	}

	public interface ChatFilter {

		/**
		 * Filters a chat message
		 * 
		 * @param player player who's message is being filtered
		 * @param msg message to filter
		 * @return the filtered message
		 */
		public String filter(Player player, String msg);
	}
}
