package dev.bcfcant.juicifruit.antlib.core.chat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.DisguiseAPI;
import dev.bcfcant.juicifruit.antlib.api.Ranks;
import dev.bcfcant.juicifruit.antlib.api.chat.ChatAPI;
import dev.bcfcant.juicifruit.antlib.api.chat.ChatPrefixer;
import dev.bcfcant.juicifruit.antlib.core.Permissions;
import dev.bcfcant.juicifruit.antlib.core.chat.censor.ChatCensor;
import dev.bcfcant.juicifruit.antlib.core.listeners.AntLibListener;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class ChatListener extends AntLibListener {

	private Map<UUID, String> lastMsgs = new HashMap<UUID, String>();

	public ChatListener(AntLib lib) {
		super(lib);
	}

	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onChat(AsyncPlayerChatEvent event) {
		if (lastMsgs.containsKey(event.getPlayer().getUniqueId()) && event.getMessage().equalsIgnoreCase(lastMsgs.get(event.getPlayer().getUniqueId()))) {
			MsgUtil.msg(event.getPlayer(), "Chat", ChatColor.GOLD + "Your message looked the same as your last one.");
			lastMsgs.put(event.getPlayer().getUniqueId(), event.getMessage());
			event.setCancelled(true);
			return;
		}
		lastMsgs.put(event.getPlayer().getUniqueId(), event.getMessage());

		String newMsg = ChatCensor.filter(event.getPlayer(), event.getMessage());
		if (newMsg == null) {
			event.setCancelled(true);
		} else {
			event.setMessage(newMsg);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onChat2(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		List<ChatPrefixer> list = ChatAPI.getPrefixers();

		Ranks rank;
		if (DisguiseAPI.isDisguised(player)) {
			rank = Ranks.REGULAR;
		} else {
			rank = Ranks.getRank(player);
		}

		StringBuilder sb = new StringBuilder();
		for (ChatPrefixer cp : list) {
			if (sb.length() > 0)
				sb.append(" ");
			sb.append(cp.getPrefix(player));
		}

		if (ChatMode.hasStaffChat(player)) {
			sb.append(ChatColor.DARK_GRAY).append("[").append(ChatColor.RED).append("Staff").append(ChatColor.DARK_GRAY).append("]");
		}

		String prefix = sb.toString();

		prefix = (prefix.isEmpty() ? "" : prefix + " ") + rank.getNameColor();

		String name = ChatColor.stripColor(player.getPlayerListName());
		String build = prefix + name;
		if (prefix.contains(ChatAPI.RAINBOW)) {
			prefix = prefix.replace(ChatAPI.RAINBOW, "");
			StringBuilder sb2 = new StringBuilder();
			sb2.append(prefix);
			for (int i = 0; i < name.length(); i++) {
				int j = i;
				if (j >= ChatAPI.RAINBOW_SCHEME.length())
					j -= ChatAPI.RAINBOW_SCHEME.length();
				sb2.append(ChatColor.COLOR_CHAR).append(ChatAPI.RAINBOW_SCHEME.charAt(j)).append(ChatColor.BOLD).append(name.charAt(i));
			}
			build = sb2.toString();
		}

		String chatMessage = event.getMessage();
		if (player.hasPermission(Permissions.CHAT_COLOUR)) {
			chatMessage = ChatColor.translateAlternateColorCodes('&', chatMessage);
		}

		String message = ChatColor.DARK_GRAY + ":" + ChatColor.RESET + " " + rank.getChatColor() + chatMessage;
		String finalMsg = build + message;

		if (ChatMode.hasStaffChat(player)) {
			for (Player p : lib.getServer().getOnlinePlayers()) {
				if (p.hasPermission(Permissions.CHAT_STAFF)) {
					p.sendMessage(finalMsg);
				}
			}
		} else {
			for (Player p : event.getRecipients()) {
				p.sendMessage(finalMsg);
			}
		}

		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.stripColor(finalMsg));
		event.setCancelled(true);
	}
}
