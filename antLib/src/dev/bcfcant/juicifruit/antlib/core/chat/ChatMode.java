package dev.bcfcant.juicifruit.antlib.core.chat;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Player;

public class ChatMode {

	private static Set<UUID> staffChat = new HashSet<UUID>();

	public static void toggleMode(Player player) {
		if (staffChat.contains(player.getUniqueId()))
			staffChat.remove(player.getUniqueId());
		else
			staffChat.add(player.getUniqueId());
	}

	public static boolean hasStaffChat(Player player) {
		return staffChat.contains(player.getUniqueId());
	}
}
