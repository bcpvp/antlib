package dev.bcfcant.juicifruit.antlib.core.chat.prefixer;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import dev.bcfcant.juicifruit.antlib.api.chat.ChatPrefixer;
import dev.bcfcant.juicifruit.antlib.core.util.plugin.PluginConnector.Connector1vs1;

public class PointsPrefixer extends ChatPrefixer {

	@Override
	public String getPrefix(Player player) {
		return ChatColor.DARK_GRAY + "[" + ChatColor.YELLOW + Connector1vs1.getPoints(player) + ChatColor.DARK_GRAY + "]";
	}

	@Override
	public ChatPriority getPriority() {
		return ChatPriority.FIRST;
	}
}
