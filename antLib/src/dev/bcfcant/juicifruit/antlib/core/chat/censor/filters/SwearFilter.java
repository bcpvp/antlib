package dev.bcfcant.juicifruit.antlib.core.chat.censor.filters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.core.chat.censor.ChatCensor.ChatFilter;

public class SwearFilter implements ChatFilter {

	private String regex = "";

	public SwearFilter(AntLib plugin) {
		List<String> list = new ArrayList<String>();

		File file = new File(plugin.getDataFolder(), "censor.txt");
		if (!file.exists()) {
			plugin.saveResource("censor.txt", false);
		}

		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String line;

			while ((line = br.readLine()) != null) {
				list.add(line);
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
				}
			}
		}

		StringBuilder sb = new StringBuilder();
		for (String w : list) {
			if (sb.length() > 0)
				sb.append("|");
			sb.append("(?i:\\b");
			StringBuilder sb2 = new StringBuilder();
			for (char c : w.toLowerCase().toCharArray()) {
				if (sb2.length() > 0)
					sb2.append("(?:\\s*)");
				sb2.append(c);
			}
			sb.append(sb2.toString());
			sb.append("\\b)");
			sb.append("|");
			sb.append("(?i:" + w.toLowerCase() + ")");
		}

		this.regex = sb.toString();
	}

	@Override
	public String filter(Player player, String msg) {
		return (regex == null ? msg : (regex.isEmpty() ? msg : msg.replaceAll(regex, "****")));
	}
}
