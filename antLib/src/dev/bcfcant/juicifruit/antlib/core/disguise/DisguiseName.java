package dev.bcfcant.juicifruit.antlib.core.disguise;

import java.util.Random;

public class DisguiseName {

	private static final String[] uppercaseArray = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "H", "I", "J", "K", "L", "M",
			"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

	private static final String[] syl1Array = new String[] { "Da", "Di", "Do", "Er", "Et", "En", "Ev", "Da", "Mi", "Est", "Os", "Ta", "Mos", "Ye", "Yi", "Za", "Zo", "Te", "Yap",
			"Moz", "Yot", "Yit", "Stu", "Mor", "Dan", "Fas", "Dul", "Fan", "Ho", "Doy", "Far", "Kor", "Fad", "Dat", "Fis", "Art", "Ant", "Ara", "Aft", "Ana", "Asa" };
	private static final String[] syl2Array = new String[] { "st", "ne", "re", "ta", "fa", "de", "ta", "do", "in", "ta", "ny", "st", "yo", "ak", "ey", "eh", "en", "et", "un",
			"ud", "us" };
	private static final String[] syl3Array = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "v", "w", "x",
			"y", "z", "an", "ra", "ts", "fs", "ad", "st", "ra" };
    private static final String[] extrasPrefix = new String[] { "_", "ez", "Its", "iTz", "iTs", "10", "Mr", "Mrs" };
    private static final String[] extrasSuffix = new String[] { "Ro", "RO", "HD", "YT", "2000", "1337", "_", "MC", "UK" };

	public static String generate() {
		Random r = new Random();

		String syl1 = syl1Array[r.nextInt(syl1Array.length - 1)];
		String syl2 = syl2Array[r.nextInt(syl2Array.length - 1)];
		String syl3 = syl3Array[r.nextInt(syl3Array.length - 1)];

        String prefix = "";
        String suffix = "";

        if (r.nextBoolean())
            prefix = extrasPrefix[r.nextInt(extrasPrefix.length - 1)];
        if (r.nextBoolean())
            suffix = extrasSuffix[r.nextInt(extrasSuffix.length - 1)];

		while (syl1.charAt(syl1.length() - 1) == syl2.charAt(0) || syl2.charAt(syl2.length() - 1) == syl3.charAt(0)) {
			syl1 = syl1Array[r.nextInt(syl1Array.length - 1)];
			syl2 = syl2Array[r.nextInt(syl2Array.length - 1)];
			syl3 = syl3Array[r.nextInt(syl3Array.length - 1)];

			syl1.toCharArray();
			syl2.toCharArray();
			syl3.toCharArray();
		}

		String name = syl1 + syl2 + syl3;

		if (name.length() < 14) {
			int extra = 14 - name.length();
			extra = r.nextInt(extra) / 2;

			for (int i = 0; i < extra; i++) {
				if (r.nextInt(100) <= 25) {
					if (r.nextBoolean()) {
						if (r.nextInt(100) <= 25) {
							name = name.concat(String.valueOf(r.nextInt(9)));
						} else {
							if (r.nextBoolean()) {
								name = uppercaseArray[r.nextInt(uppercaseArray.length - 1)].concat(name);
							} else {
								name = name.concat(uppercaseArray[r.nextInt(uppercaseArray.length - 1)]);
							}
						}
					} else {
						if (r.nextBoolean()) {
							name = syl1Array[r.nextInt(syl1Array.length - 1)].concat(name);
						} else {
							if (r.nextBoolean()) {
								name = name.concat(syl1Array[r.nextInt(syl1Array.length - 1)]);
							} else {
								name = name.concat(syl2Array[r.nextInt(syl2Array.length - 1)]);
							}
						}
					}
				}
			}
		}

        name = prefix + name + suffix;

		if (name.length() > 14)
			name = name.substring(0, 13);

		return name;
	}
}
