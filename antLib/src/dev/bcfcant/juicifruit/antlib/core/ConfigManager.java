package dev.bcfcant.juicifruit.antlib.core;

import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

import dev.bcfcant.juicifruit.antlib.api.AbstractConfigManager;
import dev.bcfcant.juicifruit.antlib.core.util.yaml.YAMLProcessor;

public class ConfigManager extends AbstractConfigManager {

	public String msgPrefix = "";

	public ConfigManager(Plugin plugin, YAMLProcessor config) {
		super(plugin, config);
	}

	public void load() {
		super.load();

		config.setComment("message-prefix", "Message prefix - %s signifies the type");
		msgPrefix = ChatColor.translateAlternateColorCodes('&', config.getString("message-prefix", "&8[&e%s&8] &6"));
	}
}
