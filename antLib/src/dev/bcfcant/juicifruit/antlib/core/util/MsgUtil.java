package dev.bcfcant.juicifruit.antlib.core.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class MsgUtil {

	private static String prefix = "";

	public static void init(String messagePrefix) {
		prefix = messagePrefix == null ? "" : messagePrefix;
	}

	/**
	 * Allows use of custom prefix.
	 */
	public static void msg(CommandSender sender, String type, String msg) {
		sender.sendMessage(prefix.replace("%s", type) + ChatColor.RESET + msg);
	}

	/**
	 * Uses default BCPVP prefix
	 */
	public static void msg(CommandSender sender, String msg) {
		msg(sender, ChatColor.YELLOW + ChatColor.BOLD.toString() + "BC" + ChatColor.RED + ChatColor.BOLD.toString() + "PVP", msg);
	}

    public static void announce(String msg, Player... exceptions) {
        List<Player> ex = Arrays.asList(exceptions);

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (ex.contains(player))
                continue;
            player.sendMessage(msg);
        }
    }

    public static void announce(String type, String msg, Player... exceptions) {
        List<Player> ex = Arrays.asList(exceptions);

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (ex.contains(player))
                continue;
            msg(player, type, msg);
        }
    }
}
