package dev.bcfcant.juicifruit.antlib.core.util.particle;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class ParticleData {

	private ParticleEffect effect;
	private float offsetX;
	private float offsetY;
	private float offsetZ;
	private float speed;
	private int amount;
	private Vector offset;

	public ParticleData(ParticleEffect effect, float offsetX, float offsetY, float offsetZ, float speed, int amount, Vector offset) {
		this.effect = effect;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.offsetZ = offsetZ;
		this.speed = speed;
		this.amount = amount;
		this.offset = offset;
	}

	public ParticleData(ParticleEffect effect, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
		this(effect, offsetX, offsetY, offsetZ, speed, amount, new Vector());
	}

	public ParticleEffect getEffect() {
		return this.effect;
	}

	public float getOffsetX() {
		return this.offsetX;
	}

	public float getOffsetY() {
		return this.offsetY;
	}

	public float getOffsetZ() {
		return this.offsetZ;
	}

	public float getSpeed() {
		return this.speed;
	}

	public int getAmount() {
		return this.amount;
	}

	public void play(Location loc) {
		effect.play(loc.clone().add(offset), offsetX, offsetY, offsetZ, speed, amount);
	}

	public void play(Location loc, double range) {
		effect.play(loc.clone().add(offset), range, offsetX, offsetY, offsetZ, speed, amount);
	}

	public void play(Player player, Location loc) {
		effect.play(player, loc.clone().add(offset), offsetX, offsetY, offsetZ, speed, amount);
	}
}
