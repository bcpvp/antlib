package dev.bcfcant.juicifruit.antlib.core.util.plugin;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.Orscrider.PvP1vs1.PvP1vs1;
import com.gmail.Orscrider.PvP1vs1.arena.GameManager;

/**
 * Connects the lib for compatibility with the different plugins, while not needing to depend on a certain class
 * (work-around imports).
 *
 * For example, the class should get data from two different plugins at runtime, and get the one that is available if
 * one of the plugins is not.
 */
public class PluginConnector {

	public static class Connector1vs1 {

		private static PvP1vs1 pvP1vs1 = (PvP1vs1) Bukkit.getPluginManager().getPlugin("1vs1");

		public static int getPoints(Player player) {
			if (Bukkit.getPluginManager().getPlugin("1vs1") != null)
				return com.gmail.Orscrider.PvP1vs1.persistence.DBConnectionController.getInstance().getScoreOfPlayer(player.getUniqueId().toString());
			return 0;
		}

		public static boolean isInGame(Player player) {
			for (Map.Entry<String, GameManager> arena : pvP1vs1.getArenaManager().getArenas().entrySet()) {
				if (arena.getValue().arenaPlayersContains(player))
					return true;
			}
			return false;
		}

		public static Player getOpponent(Player player) {
			GameManager arena = inGameOf(player);
			return arena.getArenaPlayers()[0].getUniqueId() == player.getUniqueId() ? arena.getArenaPlayers()[1] : arena.getArenaPlayers()[0];
		}

		public static GameManager inGameOf(Player player) {
			for (Map.Entry<String, GameManager> arena : pvP1vs1.getArenaManager().getArenas().entrySet()) {
				if (arena.getValue().arenaPlayersContains(player))
					return arena.getValue();
			}
			return null;
		}
	}

}
