package dev.bcfcant.juicifruit.antlib.core.util.yaml;

/**
 * YAMLProcessor exception.
 */
public class YAMLProcessorException extends Exception {

	private static final long serialVersionUID = -3182747317277455972L;

	public YAMLProcessorException() {
		super();
	}

	public YAMLProcessorException(String msg) {
		super(msg);
	}
}