package dev.bcfcant.juicifruit.antlib.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.comphenix.protocol.reflect.accessors.Accessors;
import com.comphenix.protocol.reflect.accessors.MethodAccessor;
import com.comphenix.protocol.utility.MinecraftFields;
import com.comphenix.protocol.utility.MinecraftReflection;

public class MiscUtil {

	private static Random r;
	private static MethodAccessor GET_VERSION = null;

	static {
		GET_VERSION = Accessors.getMethodAccessor(MinecraftReflection.getNetworkManagerClass(), "getVersion");
	}

	public static String formatLocation(Location loc) {
		return String.format("%d,%d,%d,%s", loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), loc.getWorld().getName());
	}

	public static Location parseLocation(String arglist) {
		String args[] = arglist.split(",");

		try {
			int x = Integer.parseInt(args[0]);
			int y = Integer.parseInt(args[1]);
			int z = Integer.parseInt(args[2]);
			World w = Bukkit.getServer().getWorld(args[3]);
			return w == null ? null : new Location(w, x, y, z);
		} catch (ArrayIndexOutOfBoundsException e) {
		} catch (NumberFormatException e) {
		}

		return null;
	}

	public static int getProtocolVersion(Player player) {
		try {
			return (Integer) GET_VERSION.invoke(MinecraftFields.getNetworkManager(player));
		} catch (Exception e) {
			return 5;
		}
	}

	/**
	 * Split the given string, but ensure single & double quoted sections of the string are kept together.
	 * <p>
	 * E.g. the String 'one "two three" four' will be split into [ "one", "two three", "four" ]
	 * 
	 * @param s the String to split
	 * @return a List of items
	 */
	public static List<String> splitQuotedString(String s) {
		List<String> matchList = new ArrayList<String>();

		Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
		Matcher regexMatcher = regex.matcher(s);

		while (regexMatcher.find()) {
			if (regexMatcher.group(1) != null) {
				matchList.add(regexMatcher.group(1));
			} else if (regexMatcher.group(2) != null) {
				matchList.add(regexMatcher.group(2));
			} else {
				matchList.add(regexMatcher.group());
			}
		}

		return matchList;
	}

	/**
	 * Return the given collection (of Comparable items) as a sorted list.
	 * 
	 * @param c the collection to sort
	 * @return a list of the sorted items in the collection
	 */
	public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
		List<T> list = new ArrayList<T>(c);
		Collections.sort(list);
		return list;
	}

	/**
	 * Picks a random value from a collection.
	 * 
	 * @param c the collection to choose from
	 * @return the chosen value
	 */
	public static <T> T pickRandom(Collection<T> c) {
		List<T> list = new ArrayList<T>(c);
		Collections.shuffle(list, r);
		return list.get(r.nextInt(list.size() - 1));
	}

	/**
	 * Get a list of all files in the given JAR (or ZIP) file within the given path, and with the given extension.
	 * 
	 * @param jarFile the JAR file to search
	 * @param path the path within the JAR file to search
	 * @param ext desired extension, may be null
	 * @return an array of path names to the found resources
	 * @throws IOException
	 */
	public static String[] listFilesinJAR(File jarFile, String path, String ext) throws IOException {
		ZipInputStream zip = new ZipInputStream(new FileInputStream(jarFile));
		ZipEntry ze = null;

		List<String> list = new ArrayList<String>();
		while ((ze = zip.getNextEntry()) != null) {
			String entryName = ze.getName();
			if (entryName.startsWith(path) && ext != null && entryName.endsWith(ext)) {
				list.add(entryName);
			}
		}
		zip.close();

		return list.toArray(new String[list.size()]);
	}

	/**
	 * @param string string to check
	 * @return if string looks like a UUID
	 */
	public static boolean looksLikeUUID(String string) {
		return string.length() == 36 && string.charAt(8) == '-' && string.charAt(13) == '-' && string.charAt(18) == '-' && string.charAt(23) == '-';
	}

	public static Random getRandom() {
		return r == null ? (r = new Random()) : r;
	}
}
