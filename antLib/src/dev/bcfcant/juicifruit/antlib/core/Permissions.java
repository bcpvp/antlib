package dev.bcfcant.juicifruit.antlib.core;

import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

public class Permissions {

	public static final Permission CHAT_COLOUR = new Permission("antlib.chat.colour", "Allows the player to chat with colour.", PermissionDefault.OP);
	public static final Permission CHAT_STAFF = new Permission("antlib.chat.staff", "Allows the player to use staff chat.", PermissionDefault.OP);

	public static final Permission VANISH_JOIN = new Permission("antlib.vanish.join", "Makes the player vanished on join", PermissionDefault.FALSE);
}
