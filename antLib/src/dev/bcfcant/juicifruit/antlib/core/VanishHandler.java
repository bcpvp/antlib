package dev.bcfcant.juicifruit.antlib.core;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.comphenix.protocol.injector.BukkitUnwrapper;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.DisguiseAPI;
import dev.bcfcant.juicifruit.antlib.api.event.PlayerVanishEvent;

public class VanishHandler {

	private AntLib lib;
	private Set<UUID> vanish = new HashSet<UUID>();

	public VanishHandler(AntLib lib) {
		this.lib = lib;
	}

	/**
	 * @return a set of vanished player uuids
	 */
	public Set<UUID> getVanishedPlayers() {
		return vanish;
	}

	/**
	 * Checks if the player is vanished
	 * 
	 * @param player player to check
	 * @return true if the player is vanished
	 */
	public boolean isVanished(Player player) {
		return getVanishedPlayers().contains(player.getUniqueId());
	}

	/**
	 * Sets the vanish status of the player
	 * 
	 * @param player player who's status to toggle
	 * @param vanish true to vanish player
	 */
	public void setVanished(Player player, boolean vanish) {
		if (vanish) {
			vanish(player);
		} else {
			unvanish(player);
		}
	}

	/**
	 * Toggles the vanish status of the player
	 * 
	 * @param player player who's status to toggle
	 */
	public void toggleVanish(Player player) {
		setVanished(player, !isVanished(player));
	}

	/**
	 * Vanishes the player
	 * 
	 * @param player player to vanish
	 */
	public void vanish(Player player) {
		for (Player p : lib.getServer().getOnlinePlayers()) {
			if (p.getUniqueId().equals(player.getUniqueId()))
				continue;
			p.hidePlayer(player);
		}
		vanish.add(player.getUniqueId());

		// Prevent entity collisions
		try {
			Object entityPlayer = BukkitUnwrapper.getInstance().unwrapItem(player);
			entityPlayer.getClass().getDeclaredField("collidesWithEntities").set(entityPlayer, false);
		} catch (Exception e) {
		}
		lib.getServer().getPluginManager().callEvent(new PlayerVanishEvent(player, true));
	}

	/**
	 * Unvanishes the player
	 * 
	 * @param player player to unvanish
	 */
	public void unvanish(Player player) {
		for (Player p : lib.getServer().getOnlinePlayers()) {
			if (p.getUniqueId().equals(player.getUniqueId()))
				continue;
			p.showPlayer(player);
		}

		// Allow entity collisions
		try {
			Object entityPlayer = BukkitUnwrapper.getInstance().unwrapItem(player);
			entityPlayer.getClass().getDeclaredField("collidesWithEntities").set(entityPlayer, true);
		} catch (Exception e) {
		}

		// Unvanishing removes the disguise so I'll add it back
		if (DisguiseAPI.isDisguised(player)) {
			DisguiseAPI.setDisguise(player, DisguiseAPI.getDisguise(player));
		}

		vanish.remove(player.getUniqueId());
		lib.getServer().getPluginManager().callEvent(new PlayerVanishEvent(player, false));
	}

	/**
	 * Refreshes the vanish state of a player
	 *
	 * @param player player to refresh
	 */
	public void refreshVanish(Player player) {
		if (isVanished(player)) {
			unvanish(player);
			vanish(player);
		}
	}

	public void onJoin(Player player) {
		for (UUID uuid : getVanishedPlayers()) {
			Player p = lib.getServer().getPlayer(uuid);
			if (p != null) {
				player.hidePlayer(p);
			}
		}
	}

	public void onLeave(Player player) {
		if (getVanishedPlayers().contains(player.getUniqueId())) {
			for (Player p : lib.getServer().getOnlinePlayers()) {
				if (p.getUniqueId().equals(player.getUniqueId()))
					continue;
				p.showPlayer(player);
			}
			vanish.remove(player.getUniqueId());
		}
		for (UUID uuid : getVanishedPlayers()) {
			if (uuid.equals(player.getUniqueId()))
				continue;

			Player p = lib.getServer().getPlayer(uuid);
			if (p != null) {
				player.showPlayer(p);
			}
		}
	}
}
