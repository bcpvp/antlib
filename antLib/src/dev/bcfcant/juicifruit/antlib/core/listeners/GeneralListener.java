package dev.bcfcant.juicifruit.antlib.core.listeners;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.DisguiseAPI;

public class GeneralListener extends AntLibListener {

	public GeneralListener(AntLib lib) {
		super(lib);
		for (World world : lib.getServer().getWorlds()) {
			world.setGameRuleValue("doDaylightCycle", "false");
			world.setTime(6000L);
			world.setStorm(false);
			world.setThundering(false);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onWeatherChange(WeatherChangeEvent event) {
		event.setCancelled(event.toWeatherState());
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onThunderChange(ThunderChangeEvent event) {
		event.setCancelled(event.toThunderState());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerJoin(PlayerJoinEvent event) {
		DisguiseAPI.handleJoin(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent event) {
		DisguiseAPI.handleLeave(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerKick(PlayerKickEvent event) {
		DisguiseAPI.handleLeave(event.getPlayer());
	}
}
