package dev.bcfcant.juicifruit.antlib.core.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleEntityCollisionEvent;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.VanishAPI;
import dev.bcfcant.juicifruit.antlib.core.Permissions;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class VanishListener extends AntLibListener {

	public VanishListener(AntLib lib) {
		super(lib);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		lib.getVanish().onJoin(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		lib.getVanish().onJoin(player);

		if (player.hasPermission(Permissions.VANISH_JOIN)) {
			VanishAPI.vanish(player);
			player.sendMessage(ChatColor.DARK_AQUA + "==============================");
			MsgUtil.msg(player, ChatColor.DARK_AQUA + "Vanish", ChatColor.AQUA + "You have join vanished and silently.");
			player.sendMessage(ChatColor.DARK_AQUA + "==============================");
			player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 2);
			event.setJoinMessage(null);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent event) {
		lib.getVanish().onLeave(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerKick(PlayerKickEvent event) {
		lib.getVanish().onLeave(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			if (!lib.getVanish().isVanished((Player) event.getEntity()))
				return;
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerDamage(EntityDamageByEntityEvent event) {
		/*
		 * Entity damaged = event.getEntity(); Entity damager = event.getDamager();
		 * 
		 * if (damaged instanceof Player) { Player player = (Player) damaged;
		 * 
		 * if (lib.getVanish().isVanished(player)) { if (damager instanceof Projectile) { Projectile projectile =
		 * (Projectile) damager; Projectile newProjectile = projectile.getWorld().spawn(projectile.getLocation(),
		 * projectile.getClass());
		 * 
		 * damaged.teleport(damaged.getLocation().add(0, 5, 0)); player.setFlying(true);
		 * 
		 * newProjectile.setBounce(projectile.doesBounce());
		 * newProjectile.setFallDistance(projectile.getFallDistance());
		 * newProjectile.setFireTicks(projectile.getFireTicks()); newProjectile.setPassenger(projectile.getPassenger());
		 * newProjectile.setShooter((ProjectileSource) projectile.getShooter());
		 * newProjectile.setTicksLived(projectile.getTicksLived()); if (!(projectile instanceof Fireball))
		 * newProjectile.setVelocity(projectile.getVelocity());
		 * 
		 * if (damager instanceof Arrow) { ((Arrow) newProjectile).setCritical(((Arrow) projectile).isCritical());
		 * ((Arrow) newProjectile).setKnockbackStrength(((Arrow) projectile).getKnockbackStrength()); } else if (damager
		 * instanceof ThrownPotion) { ((ThrownPotion) newProjectile).getEffects().clear(); ((ThrownPotion)
		 * newProjectile).getEffects().addAll(((ThrownPotion) projectile).getEffects()); ((ThrownPotion)
		 * newProjectile).setItem(((ThrownPotion) projectile).getItem()); } else if (damager instanceof Fireball) {
		 * ((Fireball) newProjectile).setDirection(((Fireball) projectile).getDirection()); if (damager instanceof
		 * WitherSkull) { ((WitherSkull) newProjectile).setCharged(((WitherSkull) projectile).isCharged()); } }
		 * 
		 * event.setCancelled(true); projectile.remove(); return; } } }
		 */
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPlayerDamage2(EntityDamageByEntityEvent event) {
		Entity damaged = event.getEntity();
		Entity damager = event.getDamager();

		if (damaged instanceof Player) {
			if (VanishAPI.isVanished((Player) damaged)) {
				event.setCancelled(true);
				return;
			}
		}

		if (damager instanceof Player) {
			if (VanishAPI.isVanished((Player) damager)) {
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
		event.setUseInteractedBlock(Result.DENY);
		event.setUseItemInHand(Result.DENY);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEntityEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerBucket(PlayerBucketFillEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerBucket(PlayerBucketEmptyEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerFish(PlayerFishEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerShearEntity(PlayerShearEntityEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerEnterBed(PlayerBedEnterEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerTargeted(EntityTargetEvent event) {
		if (!(event.getTarget() instanceof Player))
			return;
		if (!lib.getVanish().isVanished((Player) event.getTarget()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerProjectileLaunch(ProjectileLaunchEvent event) {
		if (!(event.getEntity().getShooter() instanceof Player))
			return;
		if (!lib.getVanish().isVanished((Player) event.getEntity().getShooter()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerShootBow(EntityShootBowEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		if (!lib.getVanish().isVanished((Player) event.getEntity()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerBreakHanging(HangingBreakByEntityEvent event) {
		if (!(event.getRemover() instanceof Player))
			return;
		if (!lib.getVanish().isVanished((Player) event.getRemover()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamageVehicle(VehicleDamageEvent event) {
		if (!(event.getAttacker() instanceof Player))
			return;
		if (!lib.getVanish().isVanished((Player) event.getAttacker()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDestroyVehicle(VehicleDestroyEvent event) {
		if (!(event.getAttacker() instanceof Player))
			return;
		if (!lib.getVanish().isVanished((Player) event.getAttacker()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerShootBow(VehicleEntityCollisionEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		if (!lib.getVanish().isVanished((Player) event.getEntity()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerPlaceBlock(BlockPlaceEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerBreakBlock(BlockBreakEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamageBlock(BlockDamageEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerIgniteBlock(BlockIgniteEvent event) {
        if (event.getPlayer() == null)
            return;

		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerChangeSign(SignChangeEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerChangeWorldEvent(PlayerChangedWorldEvent event) {
		if (!lib.getVanish().isVanished(event.getPlayer()))
			return;
		lib.getVanish().refreshVanish(event.getPlayer());
	}
}
