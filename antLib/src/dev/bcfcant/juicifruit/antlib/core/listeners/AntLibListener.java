package dev.bcfcant.juicifruit.antlib.core.listeners;

import org.bukkit.event.Listener;

import dev.bcfcant.juicifruit.antlib.AntLib;

public abstract class AntLibListener implements Listener {

	protected AntLib lib;

	protected AntLibListener(AntLib lib) {
		this.lib = lib;
	}
}
