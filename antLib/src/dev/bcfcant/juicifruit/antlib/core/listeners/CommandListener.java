package dev.bcfcant.juicifruit.antlib.core.listeners;

import java.util.Arrays;
import java.util.List;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

import dev.bcfcant.juicifruit.antlib.AntLib;

/**
 * Prevents other plugins from overriding AntLib commands
 */
public class CommandListener extends AntLibListener {

	private List<String> fix;

	public CommandListener(AntLib lib) {
		super(lib);
		fix = Arrays.asList("tps");
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
		String msg = event.getMessage();
		boolean slash = msg.startsWith("/");
		if (slash)
			msg = msg.substring(1);
		for (String s : fix) {
			if (msg.equalsIgnoreCase(s)) {
				msg = lib.getDescription().getName() + ":" + msg;
			}
		}
		event.setMessage((slash ? "/" : "") + msg);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onConsoleCommand(ServerCommandEvent event) {
		String cmd = event.getCommand();
		boolean slash = cmd.startsWith("/");
		if (slash)
			cmd = cmd.substring(1);
		for (String s : fix) {
			if (cmd.equalsIgnoreCase(s)) {
				cmd = lib.getDescription().getName() + ":" + cmd;
			}
		}
		event.setCommand((slash ? "/" : "") + cmd);
	}
}
