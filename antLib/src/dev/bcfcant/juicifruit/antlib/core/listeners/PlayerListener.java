package dev.bcfcant.juicifruit.antlib.core.listeners;

import dev.bcfcant.juicifruit.antlib.api.DisguiseAPI;
import dev.bcfcant.juicifruit.antlib.api.VanishAPI;
import dev.bcfcant.juicifruit.antlib.api.ground.GroundUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.Ranks;
import dev.bcfcant.juicifruit.antlib.api.TitleAPI;
import dev.bcfcant.juicifruit.antlib.core.util.plugin.PluginConnector.Connector1vs1;

public class PlayerListener extends AntLibListener {

	public PlayerListener(AntLib lib) {
		super(lib);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onJoin(PlayerJoinEvent event) {
		event.setJoinMessage(null);
		Ranks.update(event.getPlayer());

		TitleAPI.sendTabTitle(event.getPlayer(), ChatColor.translateAlternateColorCodes('&', "&bWelcome to &eBC&4PVP&8: &5PvP at it's finest!"), ChatColor.translateAlternateColorCodes('&', "&bVisit our website at: &dbcpvpmc.enjin.com"));
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onQuit(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		GroundUtil.handleDisconnect(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onKick(PlayerKickEvent event) {
		event.setLeaveMessage(null);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onFoodDecrease(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onBreakBlock(BlockBreakEvent event) {
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPlaceBlock(BlockPlaceEvent event) {
		if ((event.getPlayer().getGameMode() != GameMode.CREATIVE) && (event.getBlock().getType() != Material.FIRE) && (event.getBlock().getType() != Material.FIREBALL))
			event.setCancelled(true);
	}

	// TEMP

	@EventHandler
	public void playerFallDamageLobby(EntityDamageEvent event) {
		if (event.getEntityType() != EntityType.PLAYER) {
			return;
		}
		if (event.getCause() != EntityDamageEvent.DamageCause.FALL) {
			return;
		}
		Player player = (Player) event.getEntity();

		if (Connector1vs1.isInGame(player)) {
			event.setCancelled(true);
			return;
		}

		if (Connector1vs1.isInGame(player)) {
			event.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void foodDecrease(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void breakBlock(BlockBreakEvent event) {
		if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
			event.setCancelled(true);
	}

	@EventHandler
	public void placeBlock(BlockPlaceEvent event) {
		if ((event.getPlayer().getGameMode() != GameMode.CREATIVE) && (event.getBlock().getType() != Material.FIRE) && (event.getBlock().getType() != Material.FIREBALL))
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void pvp(EntityDamageEvent event) {
		if ((event.getEntity() instanceof Player)) {
			Player player = (Player) event.getEntity();

			if (!Connector1vs1.isInGame(player))
				event.setCancelled(true);
			else
				event.setCancelled(false);
		}
	}

	@EventHandler
	public void loginSpawn(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		player.teleport(Bukkit.getWorld("flatroom").getSpawnLocation());
	}

	@EventHandler
	public void quit(PlayerQuitEvent event) {
		// Core.instance().getBarParticipate().remove(event.getPlayer());
	}

	@EventHandler
	public void motd(ServerListPingEvent event) {
		String motd = "�8[�e�lBC�4�lPVP�r�8] �a�l1vs1�r �8/ �c�lFFA�r �8\n�5�lPVP At Its Finest!�r�8  ||  �d�lbcpvpmc.enjin.com";
		event.setMotd(motd);
	}

	@EventHandler
	public void fireSpread(BlockSpreadEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void blockMelt(BlockFadeEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void tabComplete(PlayerChatTabCompleteEvent event) {
		event.getTabCompletions().clear();

		for (Player player : Bukkit.getOnlinePlayers()) {
			String name = player.getName();

			if (DisguiseAPI.isDisguised(player))
				name = ChatColor.stripColor(DisguiseAPI.getDisguise(player));

			if (event.getTabCompletions() == null || name.toLowerCase().startsWith(event.getLastToken().toLowerCase()) && !VanishAPI.isVanished(event.getPlayer()))
				event.getTabCompletions().add(name);
		}
	}
}
