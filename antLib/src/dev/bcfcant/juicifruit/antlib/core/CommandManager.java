package dev.bcfcant.juicifruit.antlib.core;

import java.lang.reflect.Field;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;

import dev.bcfcant.juicifruit.antlib.api.AbstractCommand;

public class CommandManager {

	private static CommandMap commandMap = null;
	private static boolean accepting = false;

	public static void init() {
		if (commandMap != null) {
			throw new RuntimeException("CommandMap has already been initialised");
		}
		CommandMap map = null;
		accepting = true;

		try {
			if (Bukkit.getPluginManager() instanceof SimplePluginManager) {
				Field field = SimplePluginManager.class.getDeclaredField("commandMap");
				field.setAccessible(true);
				Object obj = field.get((SimplePluginManager) Bukkit.getPluginManager());
				if (obj instanceof CommandMap) {
					map = (CommandMap) obj;
				}
			} else {
				// Fallback attempt
				Field field = Bukkit.getPluginManager().getClass().getDeclaredField("commandMap");
				if (field != null) {
					field.setAccessible(true);
					Object obj = field.get(Bukkit.getPluginManager());
					if (obj != null && obj instanceof CommandMap) {
						map = (CommandMap) obj;
					}
				}
			}
		} catch (Exception e) {
			Bukkit.getServer().getLogger().severe("Could not initialise CommandManager: " + e.getLocalizedMessage());
			e.printStackTrace();
		}

		if (map == null) {
			return;
		}

		commandMap = map;
	}

	public static void postInit() {
		accepting = false;
	}

	public static void register(Plugin plugin, AbstractCommand command) {
		Validate.notNull(commandMap, "CommandManager has not been initialised.");
		Validate.isTrue(accepting, "Not accepting new commands. Registration should be done on plugin enable.");

		commandMap.register(command.getLabel(), plugin.getName(), command);
	}
}
