package dev.bcfcant.juicifruit.antlib.api;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginIdentifiableCommand;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.Plugin;

import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public abstract class AbstractCommand extends Command implements PluginIdentifiableCommand {

	protected final Plugin plugin;

	protected AbstractCommand(Plugin plugin, String name, String description, String permission, PermissionDefault permissionDefault) {
		super(name, description, "/" + name, new ArrayList<String>());
		this.plugin = plugin;
		this.setPermission(permission);
		this.setPermissionMessage(ChatColor.RED + "You do not have permission for this command.");
		Permission perm = new Permission(getPermission(), getDescription(), permissionDefault);
		try {
			Bukkit.getPluginManager().addPermission(perm);
		} catch (IllegalArgumentException e) { // Already registered
		}
	}

	@Override
	public final boolean execute(CommandSender sender, String commandLabel, String[] args) {
		if (!plugin.isEnabled()) {
			return false;
		}

		if (!testPermission(sender)) {
			return true;
		}

		boolean success = false;

		try {
			success = run(sender, commandLabel, args);
		} catch (Throwable ex) {
			throw new CommandException("Unhandled exception executing command '" + commandLabel + "' in plugin " + plugin.getDescription().getFullName(), ex);
		}

		if (!success && (usageMessage != null) && (usageMessage.length() > 0)) {
			sendUsageMessage(sender);
		}

		return success;
	}

	/**
	 * Executes the command, returning its success
	 *
	 * @param sender Source object which is executing this command
	 * @param label The alias of the command used
	 * @param args All arguments passed to the command, split via ' '
	 * @return true if the command was successful, otherwise false
	 */
	public abstract boolean run(CommandSender sender, String label, String[] args);

	public void sendUsageMessage(CommandSender sender) {
		sender.sendMessage(ChatColor.RED + "Usage: " + usageMessage);
	}

	@Override
	public Plugin getPlugin() {
		return plugin;
	}

	public boolean isPlayer(CommandSender sender) {
		boolean player = (sender instanceof Player);
		if (!player) {
			MsgUtil.msg(sender, ChatColor.RED + "Only in-game players can use this command.");
		}
		return player;
	}
}
