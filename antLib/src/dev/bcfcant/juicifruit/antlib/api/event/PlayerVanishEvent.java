package dev.bcfcant.juicifruit.antlib.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerVanishEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private Player player;
	private boolean vanish;

	public PlayerVanishEvent(Player who, boolean vanish) {
		this.player = who;
		this.vanish = vanish;
	}

	/**
	 * Returns the player involved in this event
	 *
	 * @return Player who is involved in this event
	 */
	public final Player getPlayer() {
		return player;
	}

	/**
	 * Gets whether the player will vanish
	 * 
	 * @return true if the player will vanish
	 */
	public boolean getVanish() {
		return vanish;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
