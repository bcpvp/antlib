package dev.bcfcant.juicifruit.antlib.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerEnterGroundEvent extends PlayerEvent {
    private static final HandlerList handlers = new HandlerList();

    public PlayerEnterGroundEvent(Player who) {
        super(who);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
