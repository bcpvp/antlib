package dev.bcfcant.juicifruit.antlib.api;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.entity.Player;
import org.spigotmc.ProtocolInjector.PacketTabHeader;
import org.spigotmc.ProtocolInjector.PacketTitle;
import org.spigotmc.ProtocolInjector.PacketTitle.Action;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.accessors.Accessors;
import com.comphenix.protocol.reflect.accessors.FieldAccessor;
import com.comphenix.protocol.reflect.accessors.MethodAccessor;
import com.comphenix.protocol.utility.MinecraftFields;
import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.core.util.MiscUtil;

public class TitleAPI {

	/**
	 * Displays a title to the player
	 * 
	 * @param player player to display to
	 * @param fadeIn fade in time in ticks
	 * @param stay stay on screen time in ticks
	 * @param fadeOut fade out time in ticks
	 * @param message message to display
	 */
	public static void sendTitle(Player player, int fadeIn, int stay, int fadeOut, String message) {
		sendTitle(player, fadeIn, stay, fadeOut, message, null);
	}

	/**
	 * Displays a subtitle to the player
	 * 
	 * @param player player to display to
	 * @param fadeIn fade in time in ticks
	 * @param stay stay on screen time in ticks
	 * @param fadeOut fade out time in ticks
	 * @param message message to display
	 */
	public static void sendSubtitle(Player player, int fadeIn, int stay, int fadeOut, String message) {
		sendTitle(player, fadeIn, stay, fadeOut, null, message);
	}

	/**
	 * Displays a title and subtitle to the player
	 * 
	 * @param player player to display to
	 * @param fadeIn fade in time in ticks
	 * @param stay stay on screen time in ticks
	 * @param fadeOut fade out time in ticks
	 * @param title title to display
	 * @param subtitle subtitle to display
	 */
	public static void sendTitle(Player player, int fadeIn, int stay, int fadeOut, String title, String subtitle) {
		if (!has1_8(player))
			return;

		PacketTitle timesPacket = new PacketTitle(Action.TIMES, fadeIn, stay, fadeOut);
		sendPacket(player, timesPacket, true);

		if (subtitle != null) {
			WrappedChatComponent wrap = WrappedChatComponent.fromJson("{\"text\": \"" + subtitle + "\"}");

			PacketTitle subtitlePacket = getTitlePacket(Action.SUBTITLE, wrap);
			if (subtitlePacket != null) {
				sendPacket(player, subtitlePacket, true);
			}
		}

		if (title != null) {
			WrappedChatComponent wrap = WrappedChatComponent.fromJson("{\"text\": \"" + title + "\"}");

			PacketTitle titlePacket = getTitlePacket(Action.TITLE, wrap);
			if (titlePacket != null) {
				sendPacket(player, titlePacket, true);
			}
		}
	}

	/**
	 * Display header or footer in the player tab list to the player
	 * 
	 * @param player player to display to
	 * @param header tab list header
	 * @param footer tab list footer
	 */
	public static void sendTabTitle(Player player, String header, String footer) {
		if (!has1_8(player))
			return;

		if (header == null)
			header = "";
		if (footer == null)
			footer = "";

		WrappedChatComponent tabHeader = WrappedChatComponent.fromJson("{\"text\": \"" + header + "\"}");
		WrappedChatComponent tabFooter = WrappedChatComponent.fromJson("{\"text\": \"" + footer + "\"}");

		PacketTabHeader packet = getTabHeaderPacket(tabHeader, tabFooter);
		if (packet != null) {
			sendPacket(player, packet, true);
		}
	}

	/**
	 * Displays a small pop-up message to the player
	 * 
	 * @param player player to display to
	 * @param message message to send
	 */
	public static void sendPopupMessage(Player player, String message) {
		if (!has1_8(player))
			return;
		PacketContainer packet = new PacketContainer(PacketType.Play.Server.CHAT);
		packet.getChatComponents().write(0, WrappedChatComponent.fromJson("{\"text\": \"" + message + "\"}"));
		packet.getBytes().write(0, (byte) 2);

		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
		} catch (InvocationTargetException e) {
			log().log(Level.WARNING, "[TitleAPI] Could not send packet to " + player.getName(), e);
		}
	}

	private static boolean has1_8(Player player) {
		return MiscUtil.getProtocolVersion(player) >= 47;
	}

	private static MethodAccessor SEND_PACKET = null;
	private static FieldAccessor TAB_HEADER_ACCESSOR = null;
	private static FieldAccessor TAB_FOOTER_ACCESSOR = null;
	private static FieldAccessor TITLE_TEXT_ACCESSOR = null;

	static {
		SEND_PACKET = Accessors.getMethodAccessor(MinecraftReflection.getNetServerHandlerClass(), "sendPacket", MinecraftReflection.getPacketClass());

		TAB_HEADER_ACCESSOR = Accessors.getFieldAccessor(PacketTabHeader.class, "header", true);
		TAB_FOOTER_ACCESSOR = Accessors.getFieldAccessor(PacketTabHeader.class, "footer", true);

		TITLE_TEXT_ACCESSOR = Accessors.getFieldAccessor(PacketTitle.class, "text", true);
	}

	private static void sendPacket(Player player, Object packet, boolean requires1_8) {
		if (!requires1_8 || has1_8(player)) {
			try {
				Object playerConnection = MinecraftFields.getPlayerConnection(player);
				SEND_PACKET.invoke(playerConnection, packet);
			} catch (Exception e) {
				log().log(Level.WARNING, "[TitleAPI] Could not send packet to " + player.getName(), e);
			}
		}
	}

	private static PacketTabHeader getTabHeaderPacket(WrappedChatComponent header, WrappedChatComponent footer) {
		try {
			PacketTabHeader packet = new PacketTabHeader();
			TAB_HEADER_ACCESSOR.set(packet, header.getHandle());
			TAB_FOOTER_ACCESSOR.set(packet, footer.getHandle());
			return packet;
		} catch (Exception e) {
			log().log(Level.WARNING, "[TitleAPI] Could not initialise tab header packet", e);
		}
		return null;
	}

	private static PacketTitle getTitlePacket(Action action, WrappedChatComponent chat) {
		try {
			PacketTitle packet = new PacketTitle(action);
			TITLE_TEXT_ACCESSOR.set(packet, chat.getHandle());
			return packet;
		} catch (Exception e) {
			log().log(Level.WARNING, "[TitleAPI] Could not initialise title packet", e);
		}
		return null;
	}

	private static AntLib lib;

	public static void init(AntLib plugin) {
		if (lib != null)
			throw new RuntimeException("TitleAPI has already been initialised");
		lib = plugin;
	}

	private static Logger log() {
		return lib.getLogger();
	}
}