package dev.bcfcant.juicifruit.antlib.api;

import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Player;

import dev.bcfcant.juicifruit.antlib.core.VanishHandler;

public class VanishAPI {

	private static VanishHandler handle;

	public static void setHandler(VanishHandler handler) {
		handle = handler;
	}

	/**
	 * @return a set of vanished player uuids
	 */
	public static Set<UUID> getVanishedPlayers() {
		return handle.getVanishedPlayers();
	}

	/**
	 * Checks if the player is vanished
	 * 
	 * @param player player to check
	 * @return true if the player is vanished
	 */
	public static boolean isVanished(Player player) {
		return handle.isVanished(player);
	}

	/**
	 * Sets the vanish status of the player
	 * 
	 * @param player player who's status to toggle
	 * @param vanish true to vanish player
	 */
	public static void setVanished(Player player, boolean vanish) {
		handle.setVanished(player, vanish);
	}

	/**
	 * Toggles the vanish status of the player
	 * 
	 * @param player player who's status to toggle
	 */
	public static void toggleVanish(Player player) {
		handle.toggleVanish(player);
	}

	/**
	 * Vanishes the player
	 * 
	 * @param player player to vanish
	 */
	public static void vanish(Player player) {
		handle.vanish(player);
	}

	/**
	 * Unvanishes the player
	 * 
	 * @param player player to unvanish
	 */
	public static void unvanish(Player player) {
		handle.vanish(player);
	}

	/**
	 * Refreshes the vanish state of a player
	 *
	 * @param player player to refresh
	 */
	public static void refreshVanish(Player player) {
		handle.refreshVanish(player);
	}
}
