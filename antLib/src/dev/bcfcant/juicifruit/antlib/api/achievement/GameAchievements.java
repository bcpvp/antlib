package dev.bcfcant.juicifruit.antlib.api.achievement;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class GameAchievements {
    private static AntLib lib;
    private static File folder;

    public static void init(AntLib antLib) {
        lib = antLib;
        folder = new File(lib.getDataFolder(), "achievements");
        folder.mkdirs();
    }

    public static boolean hasAchievement(Player player, GameAchievement achievement) {
        File file = new File(folder, player.getUniqueId().toString() + ".yml");
        if (!file.exists())
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

        FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        if (configuration.getString("achievements") == null)
            return false;

        String ach = configuration.getString("achievements");
        String[] achIds = ach.split(",");

        for (String id : achIds)
            if (achievement.getId().equals(id))
                return true;

        return false;
    }

    public static void giveAchievement(GameAchievement achievement, Player who) {
        if (hasAchievement(who, achievement))
            return;

        MsgUtil.announce("§a§l>>§r §2" + who.getName() + " §r§ahas unlocked the achievement §2§l" + achievement.getName() + "§r§a!", who);

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player == who) {
                player.playSound(player.getLocation(), Sound.PORTAL_TRAVEL, 10f, 0f);
                player.sendMessage("§b§l>>§r §3You just unlocked the achievement §l" + achievement.getName() + "§r§3! §o(" + achievement.getDescription() + ")");
                continue;
            }

            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1f, 8f);
        }

        File file = new File(folder, who.getUniqueId().toString() + ".yml");
        if (!file.exists())
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

        FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        if (configuration.getString("achievements") == null)
            configuration.set("achievements", "");

        String ach = configuration.getString("achievements");
        configuration.set("achievements", ach + "," + achievement.getId());

        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
