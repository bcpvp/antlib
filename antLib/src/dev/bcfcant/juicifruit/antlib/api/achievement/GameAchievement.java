package dev.bcfcant.juicifruit.antlib.api.achievement;

public class GameAchievement {
    private String name;
    private String id;
    private String description;

    public GameAchievement(String id, String name, String description) {
        if (id.contains(","))
            throw new IllegalArgumentException("Identifier should not contain commas.");

        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
