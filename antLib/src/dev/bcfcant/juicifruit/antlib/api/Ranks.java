package dev.bcfcant.juicifruit.antlib.api;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.chat.ChatAPI;
import dev.bcfcant.juicifruit.antlib.core.util.MiscUtil;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public enum Ranks {

	REGULAR("§e", "§r", "default", "§e", "Regular"),
	PREMIUM("§8[§b$§8] §b", "§r", "premium", "§b", "Premium"),
	VIP("§8[§dVIP§8]§r §5§l", "§r", "vip", "§5", "VIP"),
	BUILDER("§8[§dBuilder§8]§r §5§l", "§d", "builder", "§5", "Builder"),
	NAUGHTY_BOYS("§8[§d#NB§8]§r §d§l", "§r", "naughty", "§d", "Naughty Boys"),
	MODERATOR("§c", "§b", "mod", "§c", "Moderator"),
	SENIOR_MODERATOR("§4", "§b", "srmod", "§4", "Senior Moderator"),
	ADMINISTRATOR("§4§l", "§b§l", "admin", "§4§l", "Administrator"),
	DEVELOPER("§8[§bDev§8]§r " + ChatAPI.RAINBOW, "§b§l", "dev", "§b§l", "Developer"),
	OWNER("§8§l[§a§lOwner§8§l]§r " + ChatAPI.RAINBOW, "§b§l", "owner", "§a§l", "Owner");

	private String nameColor;
	private String chatColor;
	private String groupName;
	private String tabColor;
	private String name;

	Ranks(String nameColor, String chatColor, String groupName, String tabColor, String name) {
		this.nameColor = nameColor;
		this.chatColor = chatColor;
		this.groupName = groupName;
		this.tabColor = tabColor;
		this.name = name;
	}

	public String getNameColor() {
		return nameColor;
	}

	public String getChatColor() {
		return chatColor;
	}

	public String getGroupName() {
		return groupName;
	}

	public String getTabColor() {
		return tabColor;
	}

	public String getName() {
		return name;
	}

	private static AntLib lib;
	private static File file;
	private static YamlConfiguration conf;

	private static Scoreboard scoreboard;

	public static void init(AntLib plugin) {
		lib = plugin;
		file = new File(lib.getDataFolder(), "ranks.yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				lib.getLogger().severe("Could not load ranks.yml file");
				return;
			}
		}
		conf = YamlConfiguration.loadConfiguration(file);

		scoreboard = lib.getServer().getScoreboardManager().getNewScoreboard();
		for (Ranks rank : values()) {
			Team team = scoreboard.registerNewTeam(rank.toString());
			team.setPrefix(rank.getTabColor());
		}

		for (String key : conf.getKeys(false)) {
			if (MiscUtil.looksLikeUUID(key)) {
				UUID uuid = UUID.fromString(key);
				OfflinePlayer player = lib.getServer().getOfflinePlayer(uuid);
				setTeam(player);
			}
		}
	}

	private static void save() {
		try {
			conf.save(file);
		} catch (IOException e) {
			lib.getLogger().severe("Could not save ranks.yml file");
		}
	}

	private static void setTeam(OfflinePlayer player) {
		Team team = scoreboard.getTeam(getRank(player).toString());
		team.addPlayer(player);
	}

	public static Ranks getRank(OfflinePlayer player) {
		return getRank(player.getUniqueId());
	}

	public static Ranks getRank(UUID uuid) {
		String path = "ranks.player." + uuid.toString() + ".rank";
		String rankName = conf.getString(path, "");
		return rankName.isEmpty() ? Ranks.REGULAR : Ranks.valueOf(rankName);
	}

	public static void setRank(OfflinePlayer player, Ranks rank) {
		setRank(player.getUniqueId(), rank);
	}

	public static void setRank(UUID uuid, Ranks rank) {
		String path = "ranks.player." + uuid.toString() + ".rank";
		conf.set(path, rank.toString());
		save();

		if (Bukkit.getPlayer(uuid) != null) {
			Player target = Bukkit.getPlayer(uuid);
			MsgUtil.msg(target, "Rank", "Your rank has been updated.");
			update(target);
		}
	}

	public static void update(OfflinePlayer player) {
		setTeam(player);
		if (player.getPlayer() != null) {
			player.getPlayer().setScoreboard(scoreboard);

			boolean disguised = DisguiseAPI.isDisguised(player.getPlayer());

			String tabName = Ranks.getRank(player.getPlayer()).getTabColor() + (disguised ? DisguiseAPI.getDisguise(player.getPlayer()) : player.getPlayer().getName());
			if (tabName.length() > 16)
				tabName = tabName.substring(0, 15);
			player.getPlayer().setPlayerListName(tabName);

			PermissionUser user = PermissionsEx.getUser(player.getPlayer());
			for (String s : user.getParentIdentifiers()) {
				user.removeGroup(s);
			}

			PermissionGroup group = PermissionsEx.getPermissionManager().getGroup(Ranks.getRank(player.getPlayer()).getGroupName());
			user.addGroup(group);
		}
	}
}
