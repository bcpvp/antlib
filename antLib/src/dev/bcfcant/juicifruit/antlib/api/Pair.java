package dev.bcfcant.juicifruit.antlib.api;

public class Pair<A, B> {

	private A a;
	private B b;

	public Pair(A a, B b) {
		this.a = a;
		this.b = b;
	}

	public final A getA() {
		return this.a;
	}

	public final B getB() {
		return this.b;
	}

	public final void setA(A a) {
		this.a = a;
	}

	public final void setB(B b) {
		this.b = b;
	}

	@Override
	public final boolean equals(Object paramObject) {
		if (!(paramObject instanceof Pair<?, ?>))
			return false;
		Pair<?, ?> localEntry = (Pair<?, ?>) paramObject;
		Object localObject1 = getA();
		Object localObject2 = localEntry.getA();
		if ((localObject1 == localObject2) || ((localObject1 != null) && (localObject1.equals(localObject2)))) {
			Object localObject3 = getB();
			Object localObject4 = localEntry.getB();
			if ((localObject3 == localObject4) || ((localObject3 != null) && (localObject3.equals(localObject4))))
				return true;
		}
		return false;
	}

	@Override
	public final String toString() {
		return "Pair [" + getA() + ", " + getB() + "]";
	}

	@Override
	public final int hashCode() {
		return (((this.a == null) ? 0 : this.a.hashCode()) ^ ((this.b == null) ? 0 : this.b.hashCode()));
	}
}
