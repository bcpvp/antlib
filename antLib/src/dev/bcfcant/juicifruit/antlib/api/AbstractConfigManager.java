package dev.bcfcant.juicifruit.antlib.api;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.bukkit.plugin.Plugin;

import dev.bcfcant.juicifruit.antlib.core.util.yaml.YAMLProcessor;

public abstract class AbstractConfigManager {

	protected Plugin plugin;
	protected YAMLProcessor config;

	protected AbstractConfigManager(Plugin plugin, YAMLProcessor config) {
		this.plugin = plugin;
		this.config = config;
	}

	public void load() {
		try {
			config.load();
		} catch (IOException e) {
			plugin.getLogger().severe("Error loading configuration: " + e.getMessage());
			e.printStackTrace();
		}

		config.setWriteDefaults(true);

		DateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
		Calendar cal = Calendar.getInstance();
		config.setHeader("# " + plugin.getDescription().getName() + " Configuration File", "# Generated " + df.format(cal.getTime()), "# For version: "
				+ plugin.getDescription().getVersion());
	}

	public void save() {
		config.save();
	}

	public YAMLProcessor getConfig() {
		return config;
	}
}
