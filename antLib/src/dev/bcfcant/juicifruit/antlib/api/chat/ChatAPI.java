package dev.bcfcant.juicifruit.antlib.api.chat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChatAPI {

	public static final String RAINBOW = "%rainbow%";
	public static final String RAINBOW_SCHEME = "c6e23bd";

	private static List<ChatPrefixer> list = new ArrayList<ChatPrefixer>();

	public static void addPrefixer(ChatPrefixer prefixer) {
		list.add(prefixer);
	}

	public static List<ChatPrefixer> getPrefixers() {
		return Collections.unmodifiableList(list);
	}
}
