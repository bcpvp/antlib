package dev.bcfcant.juicifruit.antlib.api.chat;

import org.bukkit.entity.Player;

public abstract class ChatPrefixer implements Comparable<ChatPrefixer> {

	public abstract String getPrefix(Player player);

	public abstract ChatPriority getPriority();

	@Override
	public final int compareTo(ChatPrefixer arg0) {
		Integer i = this.getPriority().ordinal();
		Integer j = arg0.getPriority().ordinal();
		return i.compareTo(j);
	}

	public static enum ChatPriority {
		FIRST,
		NORMAL,
		LAST;
	}
}
