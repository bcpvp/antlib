package dev.bcfcant.juicifruit.antlib.api;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;

import dev.bcfcant.juicifruit.antlib.core.disguise.DisguiseName;
import dev.bcfcant.juicifruit.antlib.core.disguise.packet.WrapperPlayServerNamedEntitySpawn;
import dev.bcfcant.juicifruit.antlib.core.util.MiscUtil;

/**
 * Player disgusie API
 * <p>
 * Due to changes made by Mojang disguises will not display to 1.8 players
 */
public class DisguiseAPI {

	private static Map<UUID, String> disguises = new HashMap<UUID, String>();

	public static void disguise(Player player) {
		setDisguise(player, isDisguised(player) ? player.getName() : DisguiseName.generate());
	}

	public static void setDisguise(Player player, String name) {
		WrapperPlayServerNamedEntitySpawn spawnPacket = new WrapperPlayServerNamedEntitySpawn(player);
		String pName = Ranks.REGULAR.getTabColor() + name;
		if (pName.length() > 16)
			pName = pName.substring(0, 15);
		spawnPacket.setPlayerName(pName);

		PacketContainer destroyPacket = new PacketContainer(PacketType.Play.Server.ENTITY_DESTROY);
		destroyPacket.getIntegerArrays().write(0, new int[] { player.getEntityId() });

		if (player.getName().equals(name))
			disguises.remove(player.getUniqueId());
		else
			disguises.put(player.getUniqueId(), pName);

		for (Player p : Bukkit.getOnlinePlayers()) {
			if (!(p.getUniqueId().equals(player.getUniqueId()))) {
				if (MiscUtil.getProtocolVersion(p) <= 5) {
					if (player.getName().equals(name)) {
						// Hack to get around skins not returning to normal
						p.hidePlayer(player);
						p.showPlayer(player);
					} else {
						try {
							ProtocolLibrary.getProtocolManager().sendServerPacket(p, destroyPacket);
							spawnPacket.sendPacket(p);
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		Ranks.update(player);
	}

	public static String getDisguise(Player player) {
		return disguises.get(player.getUniqueId());
	}

	public static boolean isDisguised(Player player) {
		return disguises.containsKey(player.getUniqueId());
	}

	public static void handleJoin(Player player) {
		sendDisguises(player);
	}

	public static void handleLeave(Player player) {
		disguises.remove(player.getUniqueId());
	}

	private static void sendDisguises(Player player) {
		if (MiscUtil.getProtocolVersion(player) <= 5) {
			for (Entry<UUID, String> entry : disguises.entrySet()) {
				if (Bukkit.getPlayer(entry.getKey()) != null && !player.getUniqueId().equals(Bukkit.getPlayer(entry.getKey()).getUniqueId())) {
					WrapperPlayServerNamedEntitySpawn spawnPacket = new WrapperPlayServerNamedEntitySpawn(Bukkit.getPlayer(entry.getKey()));
					spawnPacket.setPlayerName(entry.getValue());

					PacketContainer destroyPacket = new PacketContainer(PacketType.Play.Server.ENTITY_DESTROY);
					destroyPacket.getIntegerArrays().write(0, new int[] { Bukkit.getPlayer(entry.getKey()).getEntityId() });

					try {
						ProtocolLibrary.getProtocolManager().sendServerPacket(player, destroyPacket);
						spawnPacket.sendPacket(player);
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
