package dev.bcfcant.juicifruit.antlib.api.analytics;

import java.util.ArrayList;
import java.util.List;

public class AnalyticManager {

	private List<AnalyticElement> elements;
	private static AnalyticManager instance;

	public AnalyticManager() {
		elements = new ArrayList<AnalyticElement>();
	}

	public List<AnalyticElement> getElements() {
		return elements;
	}

	public AnalyticElement getElement(String name) {
		for (AnalyticElement element : elements)
			if (element.getName().equals(name))
				return element;

		return null;
	}

	public void setElementValue(String name, Object value) {
		if (getElement(name) == null)
			throw new IllegalArgumentException("ELEMENT " + name + " IS NOT REGISTERED!");

		getElement(name).setValue(value);
	}

	public void registerElement(AnalyticElement element) {
		if (getElement(element.getName()) != null)
			throw new IllegalArgumentException("ELEMENT " + element.getName() + " IS ALREADY REGISTERED!");

		getElements().add(element);
	}

	public static AnalyticManager getInstance() {
		if (instance == null) {
			instance = new AnalyticManager();
		}

		return instance;
	}
}
