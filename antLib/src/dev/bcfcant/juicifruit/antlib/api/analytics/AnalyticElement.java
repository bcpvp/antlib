package dev.bcfcant.juicifruit.antlib.api.analytics;

public class AnalyticElement {
	private String name;
	private String displayName;
	private Object value;

	public AnalyticElement(String name, String displayName) {
		this.name = name;
		this.displayName = displayName;
	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
