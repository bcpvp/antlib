package dev.bcfcant.juicifruit.antlib.api.ground;

import dev.bcfcant.juicifruit.antlib.api.event.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class GroundTask implements Runnable{
    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!GroundUtil.lastFallDistance.containsKey(player.getUniqueId()))
                GroundUtil.lastFallDistance.put(player.getUniqueId(), 0f);

            if (GroundUtil.isGrounded(player) && !GroundUtil.lastTickGrounded.contains(player.getUniqueId())) {
                Bukkit.getPluginManager().callEvent(new PlayerEnterGroundEvent(player));
                GroundUtil.lastTickGrounded.add(player.getUniqueId());
            } else if (!GroundUtil.isGrounded(player) && GroundUtil.lastTickGrounded.contains(player.getUniqueId())) {
                Bukkit.getPluginManager().callEvent(new PlayerExitGroundEvent(player));
                GroundUtil.lastTickGrounded.remove(player.getUniqueId());
            } else if (GroundUtil.isGrounded(player) && GroundUtil.lastTickGrounded.contains(player.getUniqueId())) {
                Bukkit.getPluginManager().callEvent(new PlayerStayGroundedEvent(player));
            } else if (!GroundUtil.isGrounded(player) && !GroundUtil.lastTickGrounded.contains(player.getUniqueId())) {
                if (player.getFallDistance() > 0f) {
                    if (player.getFallDistance() != GroundUtil.lastFallDistance.get(player.getUniqueId())) {
                        Bukkit.getPluginManager().callEvent(new PlayerFallEvent(player));
                    } else {
                        Bukkit.getPluginManager().callEvent(new PlayerFloatEvent(player));
                    }
                    GroundUtil.lastFallDistance.put(player.getUniqueId(), player.getFallDistance());
                } else {
                    Bukkit.getPluginManager().callEvent(new PlayerFloatEvent(player));
                }
            }
        }
    }
}
