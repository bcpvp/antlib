package dev.bcfcant.juicifruit.antlib.api.ground;

import net.minecraft.server.v1_7_R4.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class GroundUtil {

    public static List<UUID> lastTickGrounded = new ArrayList<UUID>();
    public static HashMap<UUID, Float> lastFallDistance = new HashMap<UUID, Float>();

    public static void handleDisconnect(Player player) {
        if (lastTickGrounded.contains(player.getUniqueId()))
            lastTickGrounded.remove(player.getUniqueId());

        if (lastFallDistance.containsKey(player.getUniqueId()))
            lastFallDistance.remove(player.getUniqueId());
    }

    public static boolean isGrounded(Player who) {
        EntityPlayer player = ((CraftPlayer)who).getHandle();
        if (player.onGround)
            return true;

        Block block = who.getLocation().clone().getBlock().getRelative(BlockFace.DOWN);

        return  block.isLiquid();
    }

    public static void startTask(Plugin plugin) {
        Bukkit.getScheduler().runTaskTimer(plugin, new GroundTask(), 1l, 1l);
    }
}
