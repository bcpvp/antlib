package dev.bcfcant.juicifruit.antlib.api.game;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Match object
 */
public class Match implements Playable {
	private List<UUID> members;
	private Mode mode;
	private Game game;

	public Match(Game game) {
		this.game = game;
		members = new ArrayList<UUID>();
	}

	/**
	 * The list of players participating to the match
	 * 
	 * @return an ArrayList of UUIDs of the members
	 */
	public List<UUID> getMembers() {
		return members;
	}

	public void startMatch() {

	}

	public void endMatch() {

	}

	/**
	 * The current mode the match is set on.
	 *
	 * @return A class implementing a mode
	 */
	public Mode getActiveMode() {
		return mode;
	}

	public Game getGame() {
		return game;
	}
}
