package dev.bcfcant.juicifruit.antlib.api.game;

import java.util.List;
import java.util.UUID;

/**
 * A match that can be spectated
 * 
 * @see dev.bcfcant.juicifruit.antlib.api.game.Match
 */
public interface Watchable {

	/**
	 * A list of all the spectators currently watching the match
	 */
	public List<UUID> getSpectators();
}
