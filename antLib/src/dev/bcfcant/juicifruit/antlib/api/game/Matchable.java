package dev.bcfcant.juicifruit.antlib.api.game;

import java.util.List;

/**
 * A game that can hold a match system (PvP)
 * 
 * @see dev.bcfcant.juicifruit.antlib.api.game.Game
 */
public interface Matchable {

	/**
	 * Contains all information of possible matches in the game
	 * 
	 * @return an ArrayList of all the registered matches
	 */
	public List<Match> getMatches();

}
