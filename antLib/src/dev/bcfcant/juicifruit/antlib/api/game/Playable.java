package dev.bcfcant.juicifruit.antlib.api.game;

import java.util.List;
import java.util.UUID;

/**
 * A game that can hold players (usually implemented by mini-games)
 * 
 * @see dev.bcfcant.juicifruit.antlib.api.game.Game
 */
public interface Playable {
	/**
	 * List of members (players) playing the game
	 */
	public List<UUID> getMembers();
}
