package dev.bcfcant.juicifruit.antlib.api.game;

import java.util.Set;
import java.util.UUID;

public class GameAPI {

	private static GameAPICore CORE;

	public static void setCore(GameAPICore core) {
		if (CORE != null) {
			throw new RuntimeException("Core already set");
		}
		CORE = core;
	}

	public static Game getGame(String identifier) {
		return CORE.getGame(identifier);
	}

	public static Game getGame(UUID member) {
		return CORE.getGame(member);
	}

	public static void registerGame(Game game) {
		CORE.registerGame(game);
	}

	public static final Set<Game> getRegisteredGames() {
		return CORE.getRegisteredGames();
	}
}
