package dev.bcfcant.juicifruit.antlib.api.game;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * A plugin that can be registered to be a (mini)game
 */
public interface Game extends Playable {

	/**
	 * The identifier of the game (accessor)
	 * 
	 * @return An identifier for the game (should be unique)
	 */
	public String getIdentifier();

	/**
	 * The core plugin class associated to the plugin
	 * 
	 * @return A JavaPlugin instance
	 */
	public JavaPlugin getPlugin();

	/**
	 * The prefix used in chat notifications
	 */
	public String getPrefix();
}
