package dev.bcfcant.juicifruit.antlib.api.game;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.command.CommandSender;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class GameAPICore {
	private AntLib lib;
	private Set<Game> registeredGames = new HashSet<Game>();

	public GameAPICore(AntLib lib) {
		this.lib = lib;
	}

	// Gets rid of warnings about not being used (can remove later)
	public AntLib getLib() {
		return lib;
	}

	public Game getGame(String identifier) {
		for (Game game : getRegisteredGames())
			if (game.getIdentifier().equals(identifier))
				return game;

		return null;
	}

	public Game getGame(UUID member) {
		for (Game game : getRegisteredGames())
			if (game.getMembers().contains(member))
				return game;

		return null;
	}

	public void registerGame(Game game) {
		if (getGame(game.getIdentifier()) != null) {
			registeredGames.add(game);
		} else {
			throw new IllegalArgumentException("The game " + game.getIdentifier() + " is already registered!");
		}
	}

	public final Set<Game> getRegisteredGames() {
		return Collections.unmodifiableSet(registeredGames);
	}

	public void sendGameMessage(Game game, String message, CommandSender who) {
		MsgUtil.msg(who, game.getPrefix(), message);
	}
}
