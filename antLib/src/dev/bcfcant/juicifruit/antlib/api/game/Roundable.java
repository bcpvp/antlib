package dev.bcfcant.juicifruit.antlib.api.game;

import java.util.HashMap;
import java.util.UUID;

/**
 * A match that has a round system
 * 
 * @see dev.bcfcant.juicifruit.antlib.api.game.Match
 */
public interface Roundable {
	/**
	 * The current round index going on
	 */
	public int getCurrentRound();

	/**
	 * Get's the round wins of a player
	 * 
	 * @param member The player's to get it's round wins amount
	 * @return The amount of wins
	 */
	public int getRoundWins(UUID member);

	/**
	 * The data holder of the round wins
	 */
	public HashMap<UUID, Integer> getRounds();

	/**
	 * Adds a round win to a player Calls onRoundWin(winner) method
	 */
	public void addRound(UUID member);

	/**
	 * Fired upon player winning a round
	 */
	public void onRoundWin(UUID winner);

	/**
	 * The required round wins to win the game
	 */
	public int getMaxRounds();
}
