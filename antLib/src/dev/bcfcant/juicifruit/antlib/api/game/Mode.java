package dev.bcfcant.juicifruit.antlib.api.game;

/**
 * A custom game state (mode). Ex.: Lobby, Fight, Ending... Holds multiple settings.
 */
public interface Mode {

	public String getName();

	public String getID();

	public Match getMatch();

	public boolean isInfinite();

	public int getTicks();

	public int getStartTicks();

	public void startMode();

	public void endMode();

	public void tickMode();

	public boolean hasHealthDecrease();

	public boolean hasFoodDecrease();

	public boolean canMove();

	public boolean canPlace();

	public boolean canBreak();

	public boolean canPvP();

	public boolean canChat();

	public boolean isInvincible();
}
