package dev.bcfcant.juicifruit.antlib.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedServerPing;

public class ServerPingUtil extends PacketAdapter {
	public static boolean USE_CUSTOM_PLAYER_COUNT = false;
	public static boolean USE_CUSTOM_PLAYER_LIST = false;
	public static boolean USE_CUSTOM_VERSION = false;
	public static int PLAYER_COUNT = 0;
	public static List<String> PLAYER_LIST = new ArrayList<String>();
	public static String VERSION_NAME = "???";
	public static int VERSION_PROTOCOL = 0;

	public static void init(Plugin plugin) {
		ProtocolLibrary.getProtocolManager().addPacketListener(new ServerPingUtil(plugin));
	}

	public ServerPingUtil(Plugin plugin) {
		super(plugin, ListenerPriority.NORMAL, Arrays.asList(PacketType.Status.Server.OUT_SERVER_INFO));
	}

	@Override
	public void onPacketSending(PacketEvent event) {
		handlePing(event.getPacket().getServerPings().read(0));
	}

	private void handlePing(WrappedServerPing ping) {
		if (USE_CUSTOM_PLAYER_COUNT) {
			ping.setPlayersOnline(PLAYER_COUNT);
		}

		if (USE_CUSTOM_PLAYER_LIST) {
			List<WrappedGameProfile> profiles = new ArrayList<WrappedGameProfile>();

			for (String s : PLAYER_LIST) {
				profiles.add(new WrappedGameProfile(UUID.randomUUID(), s));
			}

			ping.setPlayers(profiles);
		}

		if (USE_CUSTOM_VERSION) {
			ping.setVersionName(VERSION_NAME);
			ping.setVersionProtocol(VERSION_PROTOCOL);
		}
	}
}
