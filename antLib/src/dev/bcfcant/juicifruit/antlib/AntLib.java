package dev.bcfcant.juicifruit.antlib;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import dev.bcfcant.juicifruit.antlib.api.achievement.GameAchievements;
import dev.bcfcant.juicifruit.antlib.api.ground.GroundUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import dev.bcfcant.juicifruit.antlib.api.Ranks;
import dev.bcfcant.juicifruit.antlib.api.ServerPingUtil;
import dev.bcfcant.juicifruit.antlib.api.VanishAPI;
import dev.bcfcant.juicifruit.antlib.api.chat.ChatAPI;
import dev.bcfcant.juicifruit.antlib.api.game.GameAPI;
import dev.bcfcant.juicifruit.antlib.api.game.GameAPICore;
import dev.bcfcant.juicifruit.antlib.command.ChatModeCommand;
import dev.bcfcant.juicifruit.antlib.command.DisguiseCommand;
import dev.bcfcant.juicifruit.antlib.command.RankCommand;
import dev.bcfcant.juicifruit.antlib.command.TpsCommand;
import dev.bcfcant.juicifruit.antlib.core.CommandManager;
import dev.bcfcant.juicifruit.antlib.core.ConfigManager;
import dev.bcfcant.juicifruit.antlib.core.VanishHandler;
import dev.bcfcant.juicifruit.antlib.core.chat.ChatListener;
import dev.bcfcant.juicifruit.antlib.core.chat.censor.ChatCensor;
import dev.bcfcant.juicifruit.antlib.core.chat.prefixer.PointsPrefixer;
import dev.bcfcant.juicifruit.antlib.core.listeners.CommandListener;
import dev.bcfcant.juicifruit.antlib.core.listeners.GeneralListener;
import dev.bcfcant.juicifruit.antlib.core.listeners.PlayerListener;
import dev.bcfcant.juicifruit.antlib.core.listeners.VanishListener;
import dev.bcfcant.juicifruit.antlib.core.util.ItemGlow;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;
import dev.bcfcant.juicifruit.antlib.core.util.yaml.YAMLFormat;
import dev.bcfcant.juicifruit.antlib.core.util.yaml.YAMLProcessor;

public class AntLib extends JavaPlugin {

	private static GameAPICore gameAPICore;

	private ConfigManager config;
	private VanishHandler vanishHandler;

	@Override
	public void onEnable() {
		if (!checkPex()) {
			this.getServer().getPluginManager().disablePlugin(this);
			this.getServer().getLogger().severe("PermissionsEx is not installed or is disabled, disabling plugin");
			return;
		}

		initConfig();

		CommandManager.init();
		Ranks.init(this);

		vanishHandler = new VanishHandler(this);
		VanishAPI.setHandler(vanishHandler);

		gameAPICore = new GameAPICore(this);
		GameAPI.setCore(gameAPICore);

		MsgUtil.init(getConfiguration().msgPrefix);

		ChatCensor.init(this);
		ItemGlow.init(this);
		ServerPingUtil.init(this);
        GameAchievements.init(this);

		initServerListUtilInfo();

		registerCommands();
		registerListeners();
		registerChatHandlers();

		for (Player player : this.getServer().getOnlinePlayers()) {
			Ranks.update(player);
		}

		new BukkitRunnable() {
			@Override
			public void run() {
				CommandManager.postInit();
			}
		}.runTaskLater(this, 1L);

		GroundUtil.startTask(this);
	}

	private void initServerListUtilInfo() {
		ServerPingUtil.USE_CUSTOM_PLAYER_LIST = true;
		ServerPingUtil.PLAYER_LIST = Arrays.asList(ChatColor.RED + ChatColor.BOLD.toString() + "BCPVP", ChatColor.DARK_AQUA + "Site: " + ChatColor.AQUA
				+ ChatColor.UNDERLINE.toString() + "http://bcpvpmc.enjin.com");
	}

	private void initConfig() {
		getDataFolder().mkdirs();
		File configFile = new File(getDataFolder(), "config.yml");
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
			} catch (IOException e) {
				getLogger().severe("Could not create configuration file: " + e.getMessage());
			}
		}

		config = new ConfigManager(this, new YAMLProcessor(configFile, true, YAMLFormat.EXTENDED));
		config.load();
	}

	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new GeneralListener(this), this);
		pm.registerEvents(new ChatListener(this), this);
		pm.registerEvents(new PlayerListener(this), this);
		pm.registerEvents(new CommandListener(this), this);
		pm.registerEvents(new VanishListener(this), this);
	}

	private void registerCommands() {
		CommandManager.register(this, new RankCommand(this));
		CommandManager.register(this, new DisguiseCommand(this));
		CommandManager.register(this, new TpsCommand(this));
		CommandManager.register(this, new ChatModeCommand(this));
	}

	private void registerChatHandlers() {
		ChatAPI.addPrefixer(new PointsPrefixer());
	}

	public ConfigManager getConfiguration() {
		return config;
	}

	private boolean checkPex() {
		Plugin pex = this.getServer().getPluginManager().getPlugin("PermissionsEx");
		if (pex == null || !pex.isEnabled()) {
			return false;
		}
		return true;
	}

	public VanishHandler getVanish() {
		return vanishHandler;
	}
}
