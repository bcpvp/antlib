package dev.bcfcant.juicifruit.antlib.command;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionDefault;

import com.comphenix.protocol.reflect.ExactReflection;
import com.comphenix.protocol.utility.MinecraftReflection;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.AbstractCommand;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class TpsCommand extends AbstractCommand {

	public TpsCommand(AntLib plugin) {
		super(plugin, "tps", "Gets the recent ticks per second for the server", "antlib.command.tps", PermissionDefault.OP);
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		try {
			Object craftServer = MinecraftReflection.getCraftBukkitClass("CraftServer").cast(plugin.getServer());
			Object mcServer = craftServer.getClass().getMethod("getServer").invoke(craftServer);
			double[] tps = (double[]) ExactReflection.fromClass(mcServer.getClass()).getField("recentTps").get(mcServer);
			StringBuilder sb = new StringBuilder();
			for (double d : tps) {
				if (sb.length() > 0)
					sb.append(", ");
				sb.append(format(d));
			}
			MsgUtil.msg(sender, "TPS", ChatColor.GOLD + "Last 1m, 5m, 15m: " + ChatColor.WHITE + sb.toString());
		} catch (Exception e) {
			MsgUtil.msg(sender, "TPS", ChatColor.RED + "Cannot obtain tps reports.");
		}

		return true;
	}

	private String format(double tps) {
		return ((tps > 16.0D) ? ChatColor.YELLOW : (tps > 18.0D) ? ChatColor.GREEN : ChatColor.RED).toString() + ((tps > 20.0D) ? "*" : "")
				+ Math.min(Math.round(tps * 100.0D) / 100.0D, 20.0D) + ChatColor.RESET;
	}
}
