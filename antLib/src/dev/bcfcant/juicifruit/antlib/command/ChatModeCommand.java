package dev.bcfcant.juicifruit.antlib.command;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.AbstractCommand;
import dev.bcfcant.juicifruit.antlib.core.chat.ChatMode;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class ChatModeCommand extends AbstractCommand {

	public ChatModeCommand(AntLib plugin) {
		super(plugin, "chatmode", "Toggles between regular and staff chat mode", "antlib.chatmode", PermissionDefault.OP);
		this.setAliases(Arrays.asList("chat", "c"));
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!isPlayer(sender)) {
			return true;
		}

		ChatMode.toggleMode((Player) sender);
		MsgUtil.msg(sender, "Chat", ChatColor.GOLD + "Toggled chat mode to " + ChatColor.RED + (ChatMode.hasStaffChat((Player) sender) ? "staff" : "global"));

		return true;
	}
}
