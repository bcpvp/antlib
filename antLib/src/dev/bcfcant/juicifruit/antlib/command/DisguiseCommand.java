package dev.bcfcant.juicifruit.antlib.command;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.AbstractCommand;
import dev.bcfcant.juicifruit.antlib.api.DisguiseAPI;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class DisguiseCommand extends AbstractCommand {

	public DisguiseCommand(AntLib plugin) {
		super(plugin, "diguise", "Disguises the player as another player.", "antlib.command.disguise", PermissionDefault.OP);
		this.setAliases(Arrays.asList("dis", "d"));
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (!(sender instanceof Player)) {
			MsgUtil.msg(sender, ChatColor.DARK_AQUA + "Disguise", ChatColor.RED + "Only players can use diguise.");
			return true;
		}
		Player player = (Player) sender;

		DisguiseAPI.disguise(player);
		MsgUtil.msg(sender, ChatColor.DARK_AQUA + "Disguise", ChatColor.AQUA
				+ (DisguiseAPI.isDisguised(player) ? "Now disguised as: " + DisguiseAPI.getDisguise(player) : "No longer disguised"));

		return true;
	}
}
