package dev.bcfcant.juicifruit.antlib.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;

import com.google.common.collect.ImmutableList;

import dev.bcfcant.juicifruit.antlib.AntLib;
import dev.bcfcant.juicifruit.antlib.api.AbstractCommand;
import dev.bcfcant.juicifruit.antlib.api.Ranks;
import dev.bcfcant.juicifruit.antlib.core.util.MsgUtil;

public class RankCommand extends AbstractCommand {

	public RankCommand(AntLib plugin) {
		super(plugin, "rank", "Sets a players rank", "antlib.command.rank", PermissionDefault.OP);
		this.setUsage("/rank <player> <rank>");
	}

	@Override
	public boolean run(CommandSender sender, String label, String[] args) {
		if (args.length < 2) {
			sendUsageMessage(sender);
			return true;
		}

		String playerName = args[0];
		String rankName = args[1];

		OfflinePlayer player = null;
		for (OfflinePlayer op : plugin.getServer().getOfflinePlayers()) {
			if (op.getName().equalsIgnoreCase(playerName)) {
				player = op;
				break;
			}
		}

		if (player == null) {
			MsgUtil.msg(sender, "Rank", ChatColor.RED + "Could not find player: " + playerName);
			return true;
		}

		Ranks rank = null;
		for (Ranks r : Ranks.values()) {
			if (r.toString().equalsIgnoreCase(rankName)) {
				rank = r;
				break;
			}
		}

		if (rank == null) {
			MsgUtil.msg(sender, "Rank", ChatColor.RED + "Unknown rank: " + rankName);
			return true;
		}

		Ranks.setRank(player.getUniqueId(), rank);
		MsgUtil.msg(sender, "Rank", ChatColor.GOLD + "Set " + ChatColor.RED + player.getName() + ChatColor.GOLD + "'s rank to " + rank.getTabColor() + rank.getName());

		return true;
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
		Validate.notNull(sender, "Sender cannot be null");
		Validate.notNull(args, "Arguments cannot be null");
		Validate.notNull(alias, "Alias cannot be null");

		if (args.length == 1) {
			Player player = sender instanceof Player ? (Player) sender : null;

			List<String> list = new ArrayList<String>();
			for (OfflinePlayer op : sender.getServer().getOfflinePlayers()) {
				if ((player == null || op.getPlayer() == null || (op.getPlayer() != null && player.canSee(op.getPlayer()))
						&& op.getName().toLowerCase().startsWith(args[0].toLowerCase()))) {
					list.add(op.getName());
				}
			}

			Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
			return list;
		}

		if (args.length == 2) {
			List<String> list = new ArrayList<String>();

			for (Ranks rank : Ranks.values()) {
				if (rank.toString().toLowerCase().startsWith(args[1].toLowerCase())) {
					list.add(rank.toString().toLowerCase());
				}
			}

			Collections.sort(list, String.CASE_INSENSITIVE_ORDER);
			return list;
		}

		return ImmutableList.of();
	}
}
